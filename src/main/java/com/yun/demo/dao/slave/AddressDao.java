package com.yun.demo.dao.slave;

import com.yun.demo.entity.slave.Address;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.jpa.repository.support.JpaEntityInformation;
import org.springframework.data.jpa.repository.support.JpaEntityInformationSupport;
import org.springframework.data.jpa.repository.support.SimpleJpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;

@Repository
public class AddressDao extends MySimpleJpaRepository<Address, Long> {

    @Autowired
    public AddressDao(@Qualifier("entityManagerSlave") EntityManager entityManager) {
        super(JpaEntityInformationSupport.getEntityInformation(Address.class, entityManager), entityManager);
    }

}

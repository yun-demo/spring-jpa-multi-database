package com.yun.demo.dao.main;

import com.yun.demo.entity.main.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.support.JpaEntityInformationSupport;
import org.springframework.data.jpa.repository.support.SimpleJpaRepository;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;

@Component
public class UserDao extends SimpleJpaRepository<User, Long> {

    @Autowired
    public UserDao(EntityManager entityManager) {
        super(JpaEntityInformationSupport.getEntityInformation(User.class, entityManager), entityManager);
    }

}

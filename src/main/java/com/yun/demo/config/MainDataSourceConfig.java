package com.yun.demo.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateSettings;
import org.springframework.boot.autoconfigure.orm.jpa.JpaProperties;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.SharedEntityManagerCreator;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.persistence.EntityManager;
import javax.sql.DataSource;
import java.util.Map;

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(entityManagerFactoryRef = "entityManagerFactoryMain"
        , transactionManagerRef = "transactionManagerMain"
        , basePackages = {"com.yun.demo.dao.main"})
public class MainDataSourceConfig {
    @Autowired
    @Qualifier("mainDataSource")
    private DataSource mainDataSource;

    @Autowired
    private JpaProperties jpaProperties;


    @Primary
    @Bean(name = "entityManagerMain")
    public EntityManager entityManager(EntityManagerFactoryBuilder builder) {
        return SharedEntityManagerCreator.createSharedEntityManager(entityManagerFactoryMain(builder).getObject());
    }
    @Primary
    @Bean(name = "entityManagerFactoryMain")
    public LocalContainerEntityManagerFactoryBean entityManagerFactoryMain (EntityManagerFactoryBuilder builder) {
        return builder
                .dataSource(mainDataSource)
                .properties(getVendorProperties())
                .packages("com.yun.demo.entity.main") //设置实体类所在位置
                .persistenceUnit("mainPersistenceUnit")
                .build();
    }

    @Primary
    @Bean(name = "transactionManagerMain")
    public PlatformTransactionManager transactionManagerMain(EntityManagerFactoryBuilder builder) {
        return new JpaTransactionManager(entityManagerFactoryMain(builder).getObject());
    }


    private Map<String, Object> getVendorProperties() {
        HibernateSettings hibernateSettings = new HibernateSettings();
        return jpaProperties.getHibernateProperties(hibernateSettings);
    }
}

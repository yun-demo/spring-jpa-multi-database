package com.yun.demo.dao.main;

import com.yun.demo.BaseTest;
import com.yun.demo.entity.main.User;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

public class UserDaoTest extends BaseTest {
    @Autowired
    private UserDao userDao;
    @Test
    public void testSave() {
        User user = new User();
        user.setId(1l);
        user.setAge(12);
        user.setName("haha");
        userDao.saveAndFlush(user);
    }
}

package com.yun.demo.dao.slave;

import com.yun.demo.BaseTest;
import com.yun.demo.entity.slave.Address;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.math.BigDecimal;

public class AddressDaoTest extends BaseTest {
    @Autowired
    private AddressDao addressDao;

    @Test
    public void testSave() {
        Address address = new Address();
        address.setId(1L);
        address.setAddress("ssssss");
        address.setAmount(BigDecimal.TEN);
        addressDao.saveAndFlush(address);
    }
}
